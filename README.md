# Hillel course Python advanced - Home work 2

### Clone my home work project from Gitlab:

    git clone git@gitlab.com:marchenko.alex/hillel_home_work_2.git

### change working directory to **hillel_home_work_2**

    cd ~/Your_path_to_the_folder/hillel_home_work_2

> also you can open terminal and drag required folder inside Terminal window


### Now you should install **pip** and **virtualenv**
### Install **pip** first
 
    sudo apt install python-pip

### Then install **virtualenv** using pip3

    pip3 install virtualenv --user

### Now create a virtual environment 

    virtualenv venv 

>you can use any name insted of **venv**

  
### Activate your virtual environment:    
    
    source venv/bin/activate

### Install necessary python libraries 

    pip install -r requirements.txt
    
### In order to deactivate virtual enviroment:

    deactivate



